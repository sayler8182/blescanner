//
//  TableViewCell.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import UIKit

// MARK: TableViewCell
class TableViewCell: UITableViewCell {
    weak var tableView: TableViewProtocol?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    func setupView() {
        // HOOK
    }
    
    class var reuseIdentifier: String {
        return "\(Self.self)"
    }
    
    class func size(_ tableView: UITableView, _ model: Any) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: TableViewProtocol
protocol TableViewProtocol: AnyObject {
    func refresh()
}

// MARK: UITableView
extension UITableView: TableViewProtocol {
    func refresh() {
        performBatchUpdates({})
    }
}
