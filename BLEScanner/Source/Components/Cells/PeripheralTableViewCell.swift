//
//  PeripheralTableViewCell.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import RxRelay
import RxSwift
import RxCocoa
import UIKit

// MARK: PeripheralTableViewCell
class PeripheralTableViewCell: TableViewCell {
    private static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter
    }()
    
    private var model: Peripheral? = nil
    private var disposeBag = DisposeBag()
    
    private let identifierLabel = Components.labels.titleLabel
        .with(text: " ")
    private let detailsLabel = Components.labels.detailsLabel
        .with(text: " ")
        .with(numberOfLines: 0)
    
    override func setupView() {
        super.setupView()
        contentView.addSubview(identifierLabel, with: [
            Anchor.to(contentView).top.offset(8),
            Anchor.to(contentView).horizontal.offset(8)
        ])
        contentView.addSubview(detailsLabel, with: [
            Anchor.to(identifierLabel).topToBottom.offset(2),
            Anchor.to(contentView).horizontal.offset(8),
            Anchor.to(contentView).bottom.offset(8)
        ])
    }
    
    func configure(_ model: Peripheral) {
        self.model = model
        identifierLabel.text = model.peripheral.identifier.uuidString
        bind(model)
    }
    
    private func bind(_ model: Peripheral) {
        disposeBag = DisposeBag()
        Observable.combineLatest(
            model.lastSeenObservable,
            model.rssiObservable)
            .observeOn(MainScheduler.asyncInstance)
            .map { (lastSeen, rssi) -> String in
                let date = Self.dateFormatter.string(from: lastSeen)
                let rssi = Int(rssi)
                return String(format: "Last seen %@ with %d dBm", date, rssi)
            }
            .do(afterNext: { [weak self] (_) in
                guard let `self` = self else { return }
                self.tableView?.refresh()
            })
            .bind(to: detailsLabel.rx.text)
            .disposed(by: disposeBag)
    }
}
