//
//  HistoryItemTableViewCell.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import RxRelay
import RxSwift
import RxCocoa
import UIKit

// MARK: HistoryItemTableViewCell
class HistoryItemTableViewCell: TableViewCell {
    private var model: HistoryItem? = nil
    private var disposeBag = DisposeBag()
    
    private let detailsLabel = Components.labels.detailsLabel
        .with(text: " ")
        .with(numberOfLines: 0)
    
    override func setupView() {
        super.setupView()
        contentView.addSubview(detailsLabel, with: [
            Anchor.to(contentView).top.offset(2),
            Anchor.to(contentView).horizontal.offset(8),
            Anchor.to(contentView).bottom.offset(2)
        ])
    }
    
    func configure(_ model: HistoryItem) {
        self.model = model
        contentView.backgroundColor = model.display.background
        detailsLabel.text = model.debugDescription
        detailsLabel.textColor = model.display.foreground
    }
}

// MARK: HistoryItem.Display
extension HistoryItem.Display {
    var background: UIColor {
        switch self {
        case .normal: return UIColor.systemBackground
        case .error: return UIColor.systemRed
        case .warning: return UIColor.systemYellow
        case .success: return UIColor.systemGreen
        }
    }
    var foreground: UIColor {
        switch self {
        case .normal: return UIColor.label
        case .error: return UIColor.black
        case .warning: return UIColor.black
        case .success: return UIColor.black
        }
    }
}
