//
//  ViewController.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import UIKit

// MARK: ViewController
class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.systemBackground
    }
}
