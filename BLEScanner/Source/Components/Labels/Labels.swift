//
//  Labels.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import Foundation
import UIKit

// MARK: Labels
extension Components {
    enum labels {
        static var statusLabel: UILabel {
            let label = UILabel()
            label.font = UIFont.boldSystemFont(ofSize: 16)
            return label
        }
        static var titleLabel: UILabel {
            let label = UILabel()
            label.font = UIFont.boldSystemFont(ofSize: 16)
            return label
        }
        static var detailsLabel: UILabel {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 14)
            return label
        }
    }
}

// MARK: UILabel
extension UILabel {
    func with(backgroundColor: UIColor) -> Self {
        self.backgroundColor = backgroundColor
        return self
    }
    func with(numberOfLines: Int) -> Self {
        self.numberOfLines = numberOfLines
        return self
    }
    func with(text: String) -> Self {
        self.text = text
        return self
    }
    func with(textAlignment: NSTextAlignment) -> Self {
        self.textAlignment = textAlignment
        return self
    }
    func with(textColor: UIColor) -> Self {
        self.textColor = textColor
        return self
    }
}
