//
//  PeripheralViewController.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import RxSwift
import UIKit

// MARK: PeripheralViewController
class PeripheralViewController: ViewController {
    private lazy var router: PeripheralRouter = PeripheralRouter(viewController: self)
    let viewModel: PeripheralViewModel = PeripheralViewModel()
    private var disposeBag = DisposeBag()
    
    private let identifierLabel = Components.labels.titleLabel
        .with(numberOfLines: 0)
        .with(text: " ")
    private let nameLabel = Components.labels.titleLabel
        .with(numberOfLines: 0)
        .with(text: " ")
    private let rssiLabel = Components.labels.titleLabel
        .with(numberOfLines: 0)
        .with(text: " ")
    private let rssi19PeakLabel = Components.labels.titleLabel
        .with(numberOfLines: 0)
        .with(textColor: UIColor.systemRed)
    private let rssi127PeakLabel = Components.labels.titleLabel
        .with(numberOfLines: 0)
        .with(textColor: UIColor.systemRed)
    private let rssiOtherPeakLabel = Components.labels.titleLabel
        .with(numberOfLines: 0)
        .with(textColor: UIColor.systemRed)
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(bottom: 44)
        tableView.separatorStyle = .none
        tableView.register(
            HistoryItemTableViewCell.self,
            forCellReuseIdentifier: HistoryItemTableViewCell.reuseIdentifier)
        return tableView
    }()
    
    private var items: [HistoryItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bind()
    }
    
    private func setupView() {
        title = "Details"
        view.addSubview(identifierLabel, with: [
            Anchor.to(view).top.safeArea.offset(8),
            Anchor.to(view).horizontal.offset(8),
        ])
        view.addSubview(nameLabel, with: [
            Anchor.to(identifierLabel).topToBottom,
            Anchor.to(view).horizontal.offset(8),
        ])
        view.addSubview(rssiLabel, with: [
            Anchor.to(nameLabel).topToBottom,
            Anchor.to(view).horizontal.offset(8),
        ])
        view.addSubview(rssi19PeakLabel, with: [
            Anchor.to(rssiLabel).topToBottom,
            Anchor.to(view).horizontal.offset(8),
        ])
        view.addSubview(rssi127PeakLabel, with: [
            Anchor.to(rssi19PeakLabel).topToBottom,
            Anchor.to(view).horizontal.offset(8),
        ])
        view.addSubview(rssiOtherPeakLabel, with: [
            Anchor.to(rssi127PeakLabel).topToBottom,
            Anchor.to(view).horizontal.offset(8),
        ])
        view.addSubview(tableView, with: [
            Anchor.to(rssiOtherPeakLabel).topToBottom.offset(8),
            Anchor.to(view).horizontal,
            Anchor.to(view).bottom
        ])
    }
    
    private func bind() {
        identifierLabel.text = viewModel.peripheral.peripheral.identifier.uuidString
        nameLabel.text = viewModel.peripheral.peripheral.name
        viewModel.peripheral
            .historyObservable
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (items) in
                guard let `self` = self else { return }
                self.items = items
                self.rssiLabel.text = String(format: "%d - correct dBm events", items.rssi)
                self.rssi19PeakLabel.text = String(format: "%d - incorrect (19 dBm) events", items.rssi19Peaks)
                self.rssi127PeakLabel.text = String(format: "%d - incorrect (127 dBm) events", items.rssi127Peaks)
                self.rssiOtherPeakLabel.text = String(format: "%d - incorrect (other dBm) dBm events", items.rssiOtherPeaks)
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)
        viewModel.isScannerAutorefreshableObservable
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (isAutorefreshable) in
                guard let `self` = self else { return }
                self.updateNavigationBar(isAutorefresh: isAutorefreshable)
            })
            .disposed(by: disposeBag)
    }
    
    private func updateNavigationBar(isAutorefresh: Bool) {
        navigationItem.rightBarButtonItems = [
            !isAutorefresh
                ? UIBarButtonItem(
                    image: UIImage(systemName: "play.circle.fill"),
                    style: .plain,
                    target: self,
                    action: #selector(scannerStartAutorefresh))
                : UIBarButtonItem(
                    image: UIImage(systemName: "stop.circle.fill"),
                    style: .plain,
                    target: self,
                    action: #selector(scannerStopAutorefresh))
        ]
    }
    
    @objc
    private func scannerStartAutorefresh() {
        viewModel.scannerStartAutorefresh()
    }
    
    @objc
    private func scannerStopAutorefresh() {
        viewModel.scannerStopAutorefresh()
    }
}

// MARK: PeripheralViewController
extension PeripheralViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: HistoryItemTableViewCell.reuseIdentifier, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? HistoryItemTableViewCell else { return }
        cell.tableView = tableView
        cell.selectionStyle = .none
        let model = items[indexPath.row]
        cell.configure(model)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = items[indexPath.row]
        return HistoryItemTableViewCell.size(tableView, model)
    }
}

// MARK: PeripheralRouter
class PeripheralRouter {
    private weak var viewController: UIViewController?
    
    init(viewController: UIViewController?) {
        self.viewController = viewController
    }
}
