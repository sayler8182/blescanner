//
//  PeripheralViewModel.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import Foundation
import RxSwift

// MARK: PeripheralViewModel
class PeripheralViewModel {
    private let scanner: Scanner = Scanner.shared
    
    var peripheral: Peripheral!
    var isScannerAutorefreshableObservable: Observable<Bool> {
        return scanner.isAutorefreshableObservable
    }
    
    init() {
        configure()
    }
    
    private func configure() { }
    
    func scannerStartAutorefresh() {
        scanner.startAutorefresh()
    }
    
    func scannerStopAutorefresh() {
        scanner.stopAutorefresh()
    }
}
