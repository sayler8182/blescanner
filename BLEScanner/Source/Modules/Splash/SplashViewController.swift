//
//  SplashViewController.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import UIKit

// MARK: SplashViewController
class SplashViewController: ViewController {
    private lazy var router: SplashRouter = SplashRouter(viewController: self)
    let viewModel: SplashViewModel = SplashViewModel()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) {
            self.router.routeToDashboard()
        }
    }
}

// MARK: SplashRouter
class SplashRouter {
    private weak var viewController: UIViewController?
    
    init(viewController: UIViewController?) {
        self.viewController = viewController
    }
    
    public func routeToDashboard() {
        let controller = DashboardViewController()
        let navigation = UINavigationController(
            rootViewController: controller)
        viewController?.window?.show(navigation, animated: true)
    }
}
