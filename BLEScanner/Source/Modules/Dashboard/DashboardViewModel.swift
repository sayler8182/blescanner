//
//  DashboardViewModel.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import Foundation
import RxRelay
import RxSwift

// MARK: DashboardViewModel
class DashboardViewModel {
    private let scanner: Scanner = Scanner.shared
    private let itemsRelay: BehaviorRelay<[Peripheral]> = .init(value: [])
    private let disposeBag = DisposeBag()
    
    var isScannerAutorefreshableObservable: Observable<Bool> {
        return scanner.isAutorefreshableObservable
    }
    var isScannerReachableObservable: Observable<Bool> {
        return scanner.isReachableObservable
    }
    var itemsObservable: Observable<[Peripheral]> {
        return itemsRelay.asObservable()
    }
    
    init() {
        configure()
    }
    
    private func configure() {
        scanner.peripheralsObservable
            .bind(to: itemsRelay)
            .disposed(by: disposeBag)
        scanner.isPoweredOnObservable
            .subscribe(onNext: { [weak self] (isPoweredOn) in
                guard let `self` = self else { return }
                self.scanner.restart()
            })
            .disposed(by: disposeBag)
    }
    
    func scannerStartAutorefresh() {
        scanner.startAutorefresh()
    }
    
    func scannerStopAutorefresh() {
        scanner.stopAutorefresh()
    }
}
