//
//  DashboardViewController.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import RxSwift
import UIKit

// MARK: DashboardViewController
class DashboardViewController: ViewController {
    private lazy var router: DashboardRouter = DashboardRouter(viewController: self)
    let viewModel: DashboardViewModel = DashboardViewModel()
    private let disposeBag = DisposeBag()
    
    private let statusView = UIView()
        .with(backgroundColor: UIColor.red)
    private let statusLabel = Components.labels.statusLabel
        .with(text: "[BLE] is disabled")
        .with(textAlignment: .center)
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(bottom: 44)
        tableView.separatorStyle = .none
        tableView.register(
            PeripheralTableViewCell.self,
            forCellReuseIdentifier: PeripheralTableViewCell.reuseIdentifier)
        return tableView
    }()
    private let statusViewHeightConnection = AnchorConnection()
    
    private var items: [Peripheral] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        bind()
    }
    
    private func setupView() {
        title = "Dashboard"
        view.addSubview(statusView, with: [
            Anchor.to(view).top.safeArea,
            Anchor.to(view).horizontal,
            Anchor.to(statusView).height(0).connect(statusViewHeightConnection).isActive(true)
        ])
        statusView.addSubview(statusLabel, with: [
            Anchor.to(statusView).top.offset(4),
            Anchor.to(statusView).horizontal.offset(8),
            Anchor.to(statusView).bottom.offset(4).priority(.defaultHigh)
        ])
        view.addSubview(tableView, with: [
            Anchor.to(statusView).topToBottom,
            Anchor.to(view).horizontal,
            Anchor.to(view).bottom
        ])
    }
    
    private func bind() {
        viewModel.isScannerReachableObservable
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (isReachable) in
                guard let `self` = self else { return }
                self.statusViewHeightConnection.constraint?.isActive = isReachable
                self.view.animation(
                    true,
                    duration: 0.3,
                    animations: { self.window?.layoutIfNeeded() })
            })
            .disposed(by: disposeBag)
        viewModel.itemsObservable
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (items) in
                guard let `self` = self else { return }
                self.items = items
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)
        viewModel.isScannerAutorefreshableObservable
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] (isAutorefreshable) in
                guard let `self` = self else { return }
                self.updateNavigationBar(isAutorefresh: isAutorefreshable)
            })
            .disposed(by: disposeBag)
    }
    
    private func updateNavigationBar(isAutorefresh: Bool) {
        navigationItem.rightBarButtonItems = [
            !isAutorefresh
                ? UIBarButtonItem(
                    image: UIImage(systemName: "play.circle.fill"),
                    style: .plain,
                    target: self,
                    action: #selector(scannerStartAutorefresh))
                : UIBarButtonItem(
                    image: UIImage(systemName: "stop.circle.fill"),
                    style: .plain,
                    target: self,
                    action: #selector(scannerStopAutorefresh))
        ]
    }
    
    @objc
    private func scannerStartAutorefresh() {
        viewModel.scannerStartAutorefresh()
    }
    
    @objc
    private func scannerStopAutorefresh() {
        viewModel.scannerStopAutorefresh()
    }
}

// MARK: DashboardViewController
extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        router.routeToPeripheral(peripheral: item)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: PeripheralTableViewCell.reuseIdentifier, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? PeripheralTableViewCell else { return }
        cell.tableView = tableView
        cell.selectionStyle = .none
        let model = items[indexPath.row]
        cell.configure(model)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = items[indexPath.row]
        return PeripheralTableViewCell.size(tableView, model)
    }
}

// MARK: DashboardRouter
class DashboardRouter {
    private weak var viewController: UIViewController?
    
    init(viewController: UIViewController?) {
        self.viewController = viewController
    }
    
    func routeToPeripheral(peripheral: Peripheral) {
        let controller = PeripheralViewController()
        controller.viewModel.peripheral = peripheral
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
