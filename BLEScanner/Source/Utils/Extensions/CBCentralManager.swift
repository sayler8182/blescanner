//
//  CBCentralManager.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import CoreBluetooth
import Foundation

// MARK: CBCentralManager
extension CBCentralManager {
    var isReachable: Bool {
        return isAvailable && isAuthorized
    }
    var isAuthorized: Bool {
        if #available(iOS 13.0, *) {
            return [CBManagerAuthorization.allowedAlways].contains(authorization)
        } else {
            let authorization = CBPeripheralManager.authorizationStatus()
            return [CBPeripheralManagerAuthorizationStatus.authorized].contains(authorization)
        }
    }
    var isAvailable: Bool {
        return [CBManagerState.poweredOn, .unknown, .resetting].contains(state)
    }
    var isPoweredOn: Bool {
        return isAuthorized && state == .poweredOn
    }
}
