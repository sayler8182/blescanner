//
//  CGSize.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import UIKit

// MARK: CGSize
public extension CGSize {
    init(size: CGFloat) {
        self.init(width: size, height: size)
    }
}
