//
//  UIWindow.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import UIKit

// MARK: UIWindow
public extension UIWindow {
    static var new: UIWindow {
        if #available(iOS 13.0, *) {
            let scene: UIWindowScene! = UIApplication.shared.connectedScenes.first as? UIWindowScene
            return UIWindow(windowScene: scene)
        } else {
            return UIWindow(frame: UIScreen.main.bounds)
        }
    }
    
    static var main: UIWindow? {
        if #available(iOS 13.0, *) {
            let scene: UIWindowScene! = UIApplication.shared.connectedScenes.first as? UIWindowScene
            let delegate: UIWindowSceneDelegate? = scene.delegate as? UIWindowSceneDelegate
            return delegate?.window!
        } else {
            let delegate: UIApplicationDelegate? = UIApplication.shared.delegate
            return delegate?.window!
        }
    }
    
    func show(_ controller: UIViewController?,
              animated: Bool) {
        self.transition(
            animated,
            duration: 0.3,
            options: [.transitionCrossDissolve],
            animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.rootViewController = controller
                UIView.setAnimationsEnabled(oldState)
        })
    }
}
