//
//  OperationQueue.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import Foundation

// MARK: OperationQueue
public extension OperationQueue {
    convenience init(maxConcurrentOperationCount: Int) {
        self.init()
        self.maxConcurrentOperationCount = maxConcurrentOperationCount
    }
}
