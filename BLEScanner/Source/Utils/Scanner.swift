//
//  Scanner.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import CoreBluetooth
import Foundation
import RxSwift
import RxRelay

// MARK: Scanner
class Scanner: NSObject {
    static let shared: Scanner = Scanner(restoreIdentifier: "shared")
    
    private let restoreIdentifier: String
    private lazy var centralManager: CBCentralManager = CBCentralManager(
        delegate: self,
        queue: queue,
        options: [
            CBCentralManagerOptionShowPowerAlertKey: true,
            CBCentralManagerOptionRestoreIdentifierKey: restoreIdentifier,
        ])
    private let peripheralUUID = CBUUID(string: "424c5545-2d42-4f4c-542d-4c4f434b0000")
    private let beeconUUID = CBUUID(string: "424c5545-2d42-4f4c-542d-424541434f4e")
    private let queue: DispatchQueue = DispatchQueue(label: "scanner", qos: .background)
    private let updateQueue: OperationQueue = OperationQueue(maxConcurrentOperationCount: 1)
    private let summary: ScannerSummary = ScannerSummary()
    private let disposeBag = DisposeBag()
    private var autorefreshDisposeBag = DisposeBag()

    private var peripherals: [UUID: Peripheral] = [:] {
        didSet {
            let value = Array(peripherals.values)
            peripheralsRelay.accept(value)
        }
    }
    private let peripheralsRelay: BehaviorRelay<[Peripheral]> = .init(value: [])
    var peripheralsObservable: Observable<[Peripheral]> {
        return peripheralsRelay.asObservable()
    }
    
    private lazy var isReachableRelay: BehaviorRelay<Bool> = .init(value: centralManager.isReachable)
    var isReachableObservable: Observable<Bool> {
        return isReachableRelay
            .asObservable()
            .distinctUntilChanged()
    }
    private lazy var isPoweredOnRelay: BehaviorRelay<Bool> = .init(value: centralManager.isPoweredOn)
    var isPoweredOnObservable: Observable<Bool> {
        return isPoweredOnRelay
            .asObservable()
            .distinctUntilChanged()
    }
    private lazy var isAutorefreshableRelay: BehaviorRelay<Bool> = .init(value: true)
    var isAutorefreshableObservable: Observable<Bool> {
        return isAutorefreshableRelay
            .asObservable()
            .distinctUntilChanged()
    }
    private lazy var isScanningRelay: BehaviorRelay<Bool> = .init(value: false)
    var isScanningObservable: Observable<Bool> {
        return isScanningRelay
            .asObservable()
            .distinctUntilChanged()
    }
    
    init(restoreIdentifier: String) {
        self.restoreIdentifier = restoreIdentifier
        super.init()
        bind()
    }
    
    private func bind() {
        Observable<Int>
            .interval(RxTimeInterval.seconds(2), scheduler: MainScheduler.asyncInstance)
            .map { [weak self] (_) -> Int in
                guard let `self` = self else { return 0 }
                return self.summary.updatesCount
            }
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                Logger.log(self.summary.debugDescription)
            })
            .disposed(by: disposeBag)
    }
}

// MARK: Scanning
extension Scanner {
    func restart() {
        guard centralManager.isPoweredOn else { return }
        stop()
        start()
    }
    
    func start() {
        guard !isScanningRelay.value else { return }
        let isAutorefreshable = isAutorefreshableRelay.value
        Logger.log("[BLE] start scanning with isAutorefreshable: \(isAutorefreshable)")
        centralManager.scanForPeripherals(
            withServices: [peripheralUUID, beeconUUID],
            options: [
                CBCentralManagerScanOptionAllowDuplicatesKey: isAutorefreshable
            ])
    }
    
    func stop() {
        guard isScanningRelay.value else { return }
        Logger.log("[BLE] stop scanning")
        centralManager.stopScan()
    }
}

// MARK: Autorefresh
extension Scanner {
    func startAutorefresh() {
        guard !isAutorefreshableRelay.value else { return }
        autorefreshDisposeBag = DisposeBag()
        let intervalObservable = Observable<Int>
            .interval(RxTimeInterval.seconds(1), scheduler: MainScheduler.asyncInstance)
        Observable.combineLatest(
            intervalObservable,
            peripheralsObservable)
            .subscribe(onNext: { (_, peripherals) in
                peripherals
                    .filter { $0.peripheral.state == .connected }
                    .forEach { $0.peripheral.readRSSI() }
            })
            .disposed(by: autorefreshDisposeBag)
        isAutorefreshableRelay.accept(true)
        restart()
    }
    
    func stopAutorefresh() {
        guard isAutorefreshableRelay.value else { return }
        autorefreshDisposeBag = DisposeBag()
        isAutorefreshableRelay.accept(false)
        restart()
    }
}

// MARK: CBCentralManagerDelegate
extension Scanner: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        Logger.log("[BLE] isReachable: \(central.isReachable)")
        Logger.log("[BLE] isPoweredOn: \(central.isPoweredOn)")
        isReachableRelay.accept(central.isReachable)
        isPoweredOnRelay.accept(central.isPoweredOn)
    }
    
    func centralManager(_ central: CBCentralManager,
                        didFailToConnect peripheral: CBPeripheral,
                        error: Error?) {
        summary.add(peripheral: peripheral, kind: .didFailToConnect, info: [.error: error])
    }
    
    func centralManager(_ central: CBCentralManager,
                        didDisconnectPeripheral peripheral: CBPeripheral,
                        error: Error?) {
        summary.add(peripheral: peripheral, kind: .didDisconnectPeripheral, info: [.error: error])
    }
    
    func centralManager(_ central: CBCentralManager,
                        didConnect peripheral: CBPeripheral) {
        summary.add(peripheral: peripheral, kind: .didConnect)
    }
    
    func centralManager(_ central: CBCentralManager,
                        didDiscover peripheral: CBPeripheral,
                        advertisementData: [String: Any],
                        rssi RSSI: NSNumber) {
        summary.add(peripheral: peripheral, kind: .didDiscover, info: [.rssi: RSSI])
        peripheral.delegate = self
        
        updateQueue.addOperation { [weak self] in
            guard let `self` = self else { return }
            if let peripheral = self.peripherals[peripheral.identifier] {
                peripheral.update(
                    rssi: RSSI.doubleValue)
            } else {
                self.peripherals[peripheral.identifier] = Peripheral(
                    peripheral: peripheral,
                    rssi: RSSI.doubleValue)
            }
        }
    }
    
    @available(iOS 13.0, *)
    func centralManager(_ central: CBCentralManager,
                        connectionEventDidOccur event: CBConnectionEvent,
                        for peripheral: CBPeripheral) {
        summary.add(peripheral: peripheral, kind: .connectionEventDidOccur)
    }
    
    @available(iOS 13.0, *)
    func centralManager(_ central: CBCentralManager,
                        didUpdateANCSAuthorizationFor peripheral: CBPeripheral) {
        summary.add(peripheral: peripheral, kind: .didUpdateANCSAuthorizationFor)
    }
    
    func centralManager(_ central: CBCentralManager,
                        willRestoreState dict: [String: Any]) {
    }
}

// MARK: CBPeripheralDelegate
extension Scanner: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral,
                    didReadRSSI RSSI: NSNumber,
                    error: Error?) {
        summary.add(peripheral: peripheral, kind: .didReadRSSI, info: [.rssi: RSSI, .error: error])
        guard let peripheral = self.peripherals[peripheral.identifier] else { return }
        peripheral.update(
            rssi: RSSI.doubleValue,
            error: error)
    }
}
