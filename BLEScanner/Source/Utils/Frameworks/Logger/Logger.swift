//
//  Logger.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import Foundation

// MARK: Loggerable
public protocol Loggerable {
    func configure()
    func log(_ entry: String)
    func log(_ datetime: String, _ entry: String)
}

// MARK: Logger
public class Logger: Loggerable {
    public static let shared = Logger()
    
    private let dispatchQueue = DispatchQueue(label: "com.bluebolt.logger")
    private let dateFormatter: DateFormatter = DateFormatter()
    private let subloggers: [Loggerable] = [
        ConsoleLogger()
    ]
    private var datetime: String {
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: Date())
    }
    
    private init() { }
    
    public func configure() {
        subloggers.forEach { $0.configure() }
    }
    
    public func log(_ entry: String) {
        log(datetime, entry)
    }
    
    public func log(_ datetime: String, _ entry: String) {
        dispatchQueue.async {
            self.subloggers.forEach { $0.log(datetime, entry) }
        }
    }
    
    public class func log(_ entry: String) {
        Self.shared.log(entry)
    }
    
    public class func log(_ datetime: String, _ entry: String) {
        Self.shared.log(datetime, entry)
    }
}
