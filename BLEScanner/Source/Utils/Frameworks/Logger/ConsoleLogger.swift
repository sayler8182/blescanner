//
//  ConsoleLogger.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import Foundation

// MARK: ConsoleLogger
public class ConsoleLogger: Loggerable {
    public func configure() { }
    
    public func log(_ entry: String) {
        print(entry)
    }
    
    public func log(_ datetime: String, _ entry: String) {
        log(entry)
    }
}
