//
//  ScannerSummary.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import CoreBluetooth
import Foundation
import RxRelay
import RxSwift

// MARK: ScannerSummary
class ScannerSummary {
    enum Kind: String, CaseIterable {
        case didFailToConnect
        case didDisconnectPeripheral
        case didConnect
        case didDiscover
        case connectionEventDidOccur
        case didUpdateANCSAuthorizationFor
        case didReadRSSI
    }
    enum Info: String, CaseIterable {
        case rssi
        case error
    }
    class Item {
        let peripheral: CBPeripheral
        let kind: Kind
        var subItems: [SubItem]
        
        init(peripheral: CBPeripheral,
             kind: Kind,
             subItems: [SubItem]) {
            self.peripheral = peripheral
            self.kind = kind
            self.subItems = subItems
        }
    }
    struct SubItem {
        let kind: Kind
        let date: Date
        let info: [Info: Any?]
        
        init(_ kind: Kind,
             _ info: [Info: Any?]) {
            self.kind = kind
            self.date = Date()
            self.info = info
        }
    }
    
    private (set) var items: [UUID: Item] = [:]
    var updatesCount: Int {
        return items.keys
            .compactMap { items[$0]?.subItems.count }
            .reduce(into: 0, { $0 += $1 })
    }
    
    func add(peripheral: CBPeripheral,
             kind: Kind,
             info: [Info: Any?] = [:]) {
        if let item = items[peripheral.identifier] {
            item.subItems.append(SubItem(kind, info))
        } else {
            items[peripheral.identifier] = Item(
                peripheral: peripheral,
                kind: kind,
                subItems: [SubItem(kind, info)])
        }
    }
}

// MARK: ScannerSummary
extension ScannerSummary: CustomDebugStringConvertible {
    var debugDescription: String {
        var description = ""
        description += "\n-----------"
        description += "\nScanner summary"
        for key in items.keys {
            description += "\n---\n"
            let item = items[key]!
            description += "  Identifier: \(key)"
            description += "\n  Name: \(item.peripheral.name ?? "")"
            for kind in Kind.allCases {
                let subItems = item.subItems.filter { $0.kind == kind }
                guard !subItems.isEmpty else { continue }
                description += "\n    \(kind.rawValue): \(subItems.count)"
                for error in subItems.compactMap({ $0.info.error }) {
                    description += "\n      \(Info.error.rawValue): \(error.localizedDescription)"
                }
            }
        }
        description += "\n-----------\n"
        return description
    }
}

// MARK: [ScannerSummary.Info: Any]
extension Dictionary where Key == ScannerSummary.Info {
    var rssi: Double? {
        let value = self[.rssi] as? NSNumber
        return value?.doubleValue
    }
    
    var error: Error? {
        return self[.error] as? Error
    }
}
