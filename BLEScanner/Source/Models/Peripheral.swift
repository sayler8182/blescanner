//
//  Peripheral.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import CoreBluetooth
import Foundation
import RxRelay
import RxSwift

// MARK: Peripheral
public class Peripheral {
    public let peripheral: CBPeripheral
    private let lastSeenRelay: BehaviorRelay<Date>
    private let discoverRssiRelay: BehaviorRelay<Double>
    private let rssiRelay: BehaviorRelay<Double>
    private let historyRelay: BehaviorRelay<[HistoryItem]>
    
    public var lastSeen: Date {
        return lastSeenRelay.value
    }
    public var lastSeenObservable: Observable<Date> {
        return lastSeenRelay.asObservable()
    }
    public var discoverRssi: Double {
        return discoverRssiRelay.value
    }
    public var discoverRssiObservable: Observable<Double> {
        return discoverRssiRelay.asObservable()
    }
    public var rssi: Double {
        return rssiRelay.value
    }
    public var rssiObservable: Observable<Double> {
        return rssiRelay.asObservable()
    }
    public var history: [HistoryItem] {
        return historyRelay.value
    }
    public var historyObservable: Observable<[HistoryItem]> {
        return historyRelay.asObservable()
    }
    
    public init(peripheral: CBPeripheral,
                rssi: Double) {
        let date = Date()
        self.peripheral = peripheral
        self.lastSeenRelay = .init(value: date)
        self.rssiRelay = .init(value: rssi)
        self.discoverRssiRelay = .init(value: rssi)
        let item = HistoryItem(
            kind: .discovered,
            date: date,
            info: [
                .rssi: rssi
            ])
        self.historyRelay = .init(value: [item])
        registerAppLifecycle()
    }
    
    public func update(rssi: Double,
                       error: Error? = nil) {
        let date = Date()
        if let error = error {
            let item = HistoryItem(
                kind: .error,
                date: date,
                info: [
                    .rssi: rssi,
                    .error: error
                ])
            lastSeenRelay.accept(date)
            rssiRelay.accept(rssi)
            addToHistory(item)
        } else {
            let item = HistoryItem(
                kind: .rssi,
                date: date,
                info: [
                    .rssi: rssi
                ])
            lastSeenRelay.accept(date)
            rssiRelay.accept(rssi)
            addToHistory(item)
        }
    }
    
    private func addToHistory(_ item: HistoryItem) {
        let history = self.history + [item]
        historyRelay.accept(history)
    }
}

// MARK: AppLifecycleable
extension Peripheral: AppLifecycleable {
    public var appLifecycleableEvents: [AppLifecycleEvent] {
        return [.willEnterForeground, .didEnterBackground, .willTerminate]
    }
    public func appLifecycleable(event: AppLifecycleEvent) {
        let date = Date()
        switch event {
        case .willEnterForeground:
            let item = HistoryItem(
                kind: .willEnterForeground,
                date: date)
            addToHistory(item)
        case .didEnterBackground:
            let item = HistoryItem(
                kind: .didEnterBackground,
                date: date)
            addToHistory(item)
        case .willTerminate:
            let item = HistoryItem(
                kind: .willTerminate,
                date: date)
            addToHistory(item)
        default: break
        }
    }
}

// MARK: HistoryItem
public struct HistoryItem {
    public enum Kind {
        case discovered
        case rssi
        case error
        case willEnterForeground
        case didEnterBackground
        case willTerminate
    }
    public enum Info: String, CaseIterable {
        case rssi
        case error
    }
    public enum Display: String, CaseIterable {
        case normal
        case error
        case warning
        case success 
    }
    
    private static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter
    }()
    
    public let kind: Kind
    public let date: Date
    public let info: [Info: Any?]
    
    public var display: Display {
        switch self.kind {
        case .discovered: return .normal
        case .rssi:
            if info.rssi < 19 {
                return .normal
            } else if info.rssi == 19 || info.rssi == 127 {
                return .warning
            } else {
                return .error
            }
        case .error: return .error
        case .willEnterForeground: return .success
        case .didEnterBackground: return .warning
        case .willTerminate: return .error
        }
    }
    
    init(kind: Kind,
         date: Date,
         info: [Info: Any?] = [:]) {
        self.kind = kind
        self.date = date
        self.info = info
    }
}

// MARK: HistoryItem
extension HistoryItem: CustomDebugStringConvertible {
    public var debugDescription: String {
        switch self.kind {
        case .discovered:
            let date = Self.dateFormatter.string(from: self.date)
            let rssi = Int(info.rssi)
            return String(format: "Discovered at %@ with %d dBm", date, rssi)
        case .rssi:
            let date = Self.dateFormatter.string(from: self.date)
            let rssi = Int(info.rssi)
            return String(format: "Updated at %@ with %d dBm", date, rssi)
        case .error:
            let date = Self.dateFormatter.string(from: self.date)
            let error = info.error.debugDescription
            let rssi = Int(info.rssi)
            return String(format: "Error at %@ with %d dBm\n*%@*", date, rssi, error)
        case .willEnterForeground:
            return "Will enter foreground"
        case .didEnterBackground:
            return "Did enter background"
        case .willTerminate:
            return "Will terminate"
        }
    }
}

// MARK: [HistoryItem]
extension Array where Element == HistoryItem {
    var rssi: Int {
        return self
            .compactMap { $0.info.rssi }
            .count
    }
    var rssi19Peaks: Int {
        return self
            .compactMap { $0.info.rssi }
            .filter { $0 == 19 }
            .count
    }
    var rssi127Peaks: Int {
        return self
            .compactMap { $0.info.rssi }
            .filter { $0 == 127 }
            .count
    }
    var rssiOtherPeaks: Int {
        return self
            .compactMap { $0.info.rssi }
            .filter { $0 >= 19 && $0 != 19 && $0 != 127 }
            .count
    }
}

// MARK: [HistoryItem.Info: Any]
extension Dictionary where Key == HistoryItem.Info {
    var rssi: Double! {
        let value = self[.rssi] as? NSNumber
        return value?.doubleValue
    }
    
    var error: Error! {
        return self[.error] as? Error
    }
}
