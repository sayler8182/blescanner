//
//  SceneDelegate.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import UIKit

// MARK: SceneDelegate
@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        guard windowScene.activationState != .background else { return }
        
        // Root
        let window: UIWindow = UIWindow(windowScene: windowScene)
        window.rootViewController = SplashViewController()
        self.window = window
        window.makeKeyAndVisible()
    }
}
