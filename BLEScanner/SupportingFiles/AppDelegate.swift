//
//  AppDelegate.swift
//  BLEScanner
//
//  Created by Konrad on 12/23/20.
//

import UIKit
import RxCocoa
import RxRelay
import RxSwift
import RxSwiftExt

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Root
        if #available(iOS 13.0, *) {
        } else if application.applicationState != .background else {
            let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = SplashViewController()
            self.window = window
            window.makeKeyAndVisible()
        }
        
        return true
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication,
                     configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
